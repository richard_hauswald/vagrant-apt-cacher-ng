#!/usr/bin/env ruby
# coding: utf-8
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "ubuntu/trusty64"
  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--memory", "400", "--nictype1", "virtio", "--nictype2", "virtio"]
  end
  config.vm.network "forwarded_port", guest: 3142, host: 3142
  config.vm.network "private_network", ip: "10.43.0.3"
  config.persistent_storage.enabled = true
  config.persistent_storage.location = "#{Dir.home}/.devlab/persistent-storages/apt-cacher-ng.vdi"
  config.persistent_storage.size = 10000
  config.persistent_storage.mountname = 'persistent-storage'
  config.persistent_storage.filesystem = 'ext4'
  config.persistent_storage.mountpoint = '/persistent-storage'
  config.persistent_storage.use_lvm = false

  config.vm.provision "shell", inline: "df -h"
  config.vm.provision "shell", inline: "apt-get update && apt-get install -y apt-cacher-ng"
  config.vm.provision "shell", inline: "service apt-cacher-ng stop"
  config.vm.provision "shell", inline: "sudo sed -i '/^Remap-gentoo/d' /etc/apt-cacher-ng/acng.conf"
  config.vm.provision "shell", inline: "sudo sed -i '/^PfilePattern/d' /etc/apt-cacher-ng/acng.conf"
  config.vm.provision "shell", inline: "sudo sed -i '/^RequestAppendix/d' /etc/apt-cacher-ng/acng.conf"
  config.vm.provision "shell", inline: 'echo \'PfilePattern = .*(\.d?deb|\.rpm|\.drpm|\.dsc|\.tar(\.gz|\.bz2|\.lzma|\.xz)(\.gpg|\?AuthParam=.*)?|\.diff(\.gz|\.bz2|\.lzma|\.xz)|\.jigdo|\.template|changelog|copyright|\.udeb|\.debdelta|\.diff/.*\.gz|(Devel)?ReleaseAnnouncement(\?.*)?|[a-f0-9]+-(susedata|updateinfo|primary|deltainfo).xml.gz|fonts/(final/)?[a-z]+32.exe(\?download.*)?|/dists/.*/installer-[^/]+/[0-9][^/]+/images/.*)$\' >> /etc/apt-cacher-ng/acng.conf'
  config.vm.provision "shell", inline: "echo 'RequestAppendix: Cookie: oraclelicense=a' >> /etc/apt-cacher-ng/acng.conf"
  config.vm.provision "shell", inline: "[ -e /persistent-storage/apt-cacher-ng/ ] || mkdir /persistent-storage/apt-cacher-ng"
  config.vm.provision "shell", inline: "chown -R apt-cacher-ng:apt-cacher-ng /persistent-storage/apt-cacher-ng/"
  config.vm.provision "shell", inline: "rm -rf /var/cache/apt-cacher-ng"
  config.vm.provision "shell", inline: "ln -s /persistent-storage/apt-cacher-ng /var/cache/apt-cacher-ng"
  config.vm.provision "shell", inline: "service apt-cacher-ng start"
  config.vm.provision "shell", inline: "df -h"
  config.vm.provision "shell", inline: "free -m"
end
